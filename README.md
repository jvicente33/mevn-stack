# MEVN Stack
Este proyecto está destinado a ser utilizado como un inicio rápido para construir un
[**M**ongo](https://www.mongodb.com/) [**E**xpress](https://expressjs.com/) [**V**ueJS](https://vuejs.org/) [**N**ode](https://nodejs.org/en/) stack. Es similar a MEAN, excepto que se ha intercambiado Angular por una aplicación de una sola página VueJS presentada en el lado del cliente.

Este es también el código utilizado en el segundo entrenamiento VueJS en la fábrica de aplicaciones UW-Parkside.

## Tecnologia
Este proyecto usa:

[Mongo](https://www.mongodb.com/) for a NoSQL database.

[Express](https://expressjs.com/) For an HTTP Server

[VueJS](https://vuejs.org/) For Views, with the [Vuetify](https://vuetifyjs.com/) Material Design Framework

[Node](https://nodejs.org/en/) For a JavaScript runtime

## Instalación

To install this project simply clone or download the repo:

`git clone`

`cd <dir name>`

`npm install`

`cp .env.example .env` **El cambio de la variable PORT en .env requerirá que lo cambie en el archivo `views/config/http.js` .**

### Setup

`npm run dev:serve`
`npm run dev:client` 

### Project Structure

##### Backend

`/src`

`--/controllers/`-- Contains controllers for our API resources.

`--/database/`

`----/models/`-- Contains the models for our API Resources using [Mongoose](http://mongoosejs.com/).

`--/middleware/`-- Any middleware you may need can go here.

`--/routes/`-- All route definitions are here.

`----/api.js`-- Routes for the API.

`----/user.js`-- Routes specific to the user resource.

##### Frontend

`/views`

`--/config/http.js`-- Axios config for local request 

`--/pages/`-- Separate Component Pages go here.

`--/router/index.js`-- Config for [vue-router](https://github.com/vuejs/vue-router)

`--/App.vue`-- Component that has Nav-Drawer, Footer, and Toolbar wrapped around a router view of other components.

`--/main.js`-- Registers the Vue components and Router

`--/index.html`-- The file we return, has the Vue app in it.

### Dependencias 

* Dependencias Via NPM
	* [Axios](https://github.com/axios/axios) For client side HTTP requests
	* [cors](https://github.com/expressjs/cors) For CORS during development
	* [dotenv](https://github.com/motdotla/dotenv) Loads our .env variables
	* [vue](https://vuejs.org/) Realtime data binding on the frontend
	* [vuetify](https://vuetifyjs.com/vuetify/quick-start) Material design for Vue
	* [vue-router](https://github.com/vuejs/vue-router) Router for the SPA 

### Routes

All user endpoints are behind the `/api` endpoint.

#### `GET`
`/users` - returns a list of all users inside of an array called `data`.

`/users/:id` - where `:id` is the id of a `user` resource. The resource is then returned in JSON format.

#### `POST`
`/users` - Creates a new `user` resource based on the payload of the request.

#### `DELETE`
`/users/:id` - Delete a user resouce matching the `:id` specified.

#### `PUT`
`/users` - Update a user based on the payload of the request


